# express-mongoose

Simple app to test mongodb Cloud Atlas CRUD operations using moongose.

## Installation

```bash
npm intall
```

## Configuration

```bash
in the / folder create a .env file with this two vars:
MONGODB_NAME="your mongodb database name"
MONGODB_URI="your mongodb connection uri"
```

## Usage

```bash
nodemon app
```

## License
[GNU GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
