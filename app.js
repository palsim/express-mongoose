const dotenv = require('dotenv').config();
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var Book = require('./Book.model');
var port = 8080;
const dbName = process.env.MONGODB_NAME;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// cloud atlas connection
const MongoClient = require('mongodb').MongoClient;
const client = new MongoClient(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true });
client.connect();


//http://localhost:8080/books
app.get('/books', async (req, res) => {
	console.log('getting all books');
	books = [];

	const db = client.db(dbName);
	const col = db.collection("books");
	
	//find all books
	const cursor = await col.find({});		
	await cursor.forEach(book => books.push(book));
	res.json(books);
});

//http://localhost:8080/book/60b2662e6d029d23d14491e8
	app.get('/book/:id', async (req, res) => {
	
	var id = new mongoose.Types.ObjectId(req.params.id);
	console.log('getting book:', id);
	
	const db = client.db(dbName);
	const col = db.collection("books");
	
	//find onde book
	col.findOne({_id: id}).then((book) => {	
		if(book){
			res.json(book);
		}
		else{
			res.send("book not found");
		}
	}).catch((err) => {
		res.send(err);
	});	
});

//http://localhost:8080/book
app.post('/book', async (req, res) => {
	console.log('adding a new book');

	const db = client.db(dbName);
	const col = db.collection("books");

	var newBook = new Book();
	newBook.title = req.body.title;
	newBook.author = req.body.author;
	newBook.category = req.body.category;
	
	//insert one book
	col.insertOne(newBook).then((result) => {
		if(result) {
			var str = `${result.insertedCount} document were inserted with the _id: ${result.insertedId}`;
			console.log(str);
			res.json(str);
		}
		else {
			res.send("book not added");
		}
	});
	
});

//http://localhost:8080/book/60b2662e6d029d23d14491e8
app.put('/book/:id', async (req, res) => {
	var id = new mongoose.Types.ObjectId(req.params.id);
	console.log('updating a book:', id);

	const db = client.db(dbName);
	const col = db.collection("books");	

	const filter = { _id: id };
	const update = { $set: req.body };
	const options = { new: true, upsert: false, returnOriginal: false };		

	//update one book
	col.findOneAndUpdate(filter, update, options).then((result) => {
		if(result && result.value) {
			res.json(result.value);
		}
		else {
			res.send("book not updated");
		}
	});
});

//http://localhost:8080/book/60b276680cadf0488cf3c47e
app.delete('/book/:id', async (req, res) => {
	var id = new mongoose.Types.ObjectId(req.params.id);
	console.log('deleting a book:', id);

	const db = client.db(dbName);
	const col = db.collection("books");	

	const filter = { _id: id };

	//delete one book
	col.findOneAndDelete(filter).then((result) => {
		if(result && result.value) {
			res.json(result.value);
		}
		else {
			res.send("book not deleted");
		}
	});
});

app.listen(port, () => {
	console.log('server started at port ' + port);
});
